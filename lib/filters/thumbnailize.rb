class Thumbnailize < Nanoc::Filter

  identifier :thumbnailize
  type       :binary

  def run(filename, params={})
    # command line should look like this for 100x100 thumbnail
    # see explanation: http://www.imagemagick.org/Usage/resize/#space_fill
    # convert IMG_0581.JPG -resize x200 -resize '200x<'  -resize 50% -gravity center -crop 100x100+0+0 +repage cut_to_fit.jpg
    width = params[:width]
    dwidth = width*2

    system([  'convert',
             filename,
             '-resize',
             "x#{dwidth}",
             '-resize',
             "'#{dwidth}x<'",
             "-resize",
             "50%",
             "-gravity",
             "center",
             "-crop",
             "#{width}x#{width}+0+0",
             "+repage",
             output_filename
           ].join(" "))
  end

end
